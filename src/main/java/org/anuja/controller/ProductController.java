package org.anuja.controller;

import org.anuja.data.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

	@Autowired
	Product product;
	
	@GetMapping(path="/api/product")
	public Product getProduct() {
		
		product.productName="Pencil";
		product.price=(double) 5;
		product.quantity=200;
		
		product.category.name="Stationary";
		
		product.variation.VariationList.add("Black color");
		product.variation.VariationList.add("Pink Color");
		product.variation.VariationList.add("Blue Color");
		
		return product;

		
	}
}
