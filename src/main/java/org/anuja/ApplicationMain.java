package org.anuja;

import org.anuja.data.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationMain {


	public static void main(String[] args) {
		ConfigurableApplicationContext context=SpringApplication.run(ApplicationMain.class, args);
		Product product =context.getBean(Product.class);
		
		product.productName="Rubber";
		product.price=(double) 5;
		product.quantity=10;
		
		product.category.name="Stationary";
		product.variation.VariationList.add("Red color");
		product.variation.VariationList.add("Pink Color");
		product.variation.VariationList.add("Gray Color");
		
		product.printData();
		
		
	}
	
}
