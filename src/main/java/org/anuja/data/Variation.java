package org.anuja.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Variation {

	@Autowired
	public List<String> VariationList;
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
    	VariationList.forEach(str -> sb.append(str+" "));
		return sb.toString();
		
	}
	public void add(String variationName) {
		VariationList.add(variationName);
	}
}
