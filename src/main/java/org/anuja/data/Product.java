package org.anuja.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Product {

	public String productName;
	public Double price;
	public Integer quantity;
	
	@Autowired
	public Category category;
	
	@Autowired
	public Variation variation;
	
	public void printData() {
		System.out.println("Product[productName="+productName+",price="+price+",quantity="+quantity+",Category="+category+",Variation="+variation+"");
		
	}
}
